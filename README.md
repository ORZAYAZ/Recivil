# Recivil

## Projeitos
### Transparência de chamados
- Uma página a qual o(a) oficial(a) pode acessar pelo Recivil para verificar a situação de seu(s) chamado(s) em aberto, assim como demais opções, para acessar está página será por meio de um botão:
```
        +---------------------------+
        |                           |
        |         CHAMADOS          |
        |                           |
        +---------------------------+
```
- Ao acessar a página, será possível conferir cada chamado em aberto para tal serventia e observar o número de OSs abertos e a quantidade de realizados no período de um dia/semana/mês.
- É fundamental que ao selecionar o chamado, ele tenha acesso a informações como:
    * Atribuição
    * Tempo de espera
    * Formas de contato
    * Atualizações (notas)

